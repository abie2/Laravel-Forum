<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use CommentableTrait, LikeableTrait;

    protected $fillable = [
        'body', 'user_id', 'commentable_id', 'commentable_type',
    ];

    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * sebuah comment dimiliki oleh seorang user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * [MOVE] ke CommentableTrait
     */
    // public function comments()
    // {
    //     return $this->morphMany('App\Comment', 'commentable')->latest();
    // }
}

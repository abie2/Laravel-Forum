<?php

namespace App;

use Auth;

trait CommentableTrait
{
    /**
     * Ini sebelumnya ada di model Thread dan Comment
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable')->latest();
    }

    /**
     * function addComment digunakan untuk comment dan reply
     */
    public function addComment($body)
    {
        $comment = new Comment;
        $comment->body = $body;
        $comment->user_id = Auth::user()->id;

        /** Menggunakan relasi polymorphism */
        $this->comments()->save($comment);

        return $comment;
    }
}
<?php

namespace App\Http\Controllers;

use Auth;
use App\Thread;
use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Untuk comment pada thread
     * Class yang ikut berpreran Thread & Comment
     */
    public function addThreadComment(Request $request, Thread $thread)
    {
        $this->validate($request, [
            'body' => 'required',
        ]);

        // $thread = Thread::findOrFail($id);

        /**
         * [MOVE] ke CommentableTrait
         */
        // $comment = new Comment;
        // $comment->body = $request->body;
        // $comment->user_id = Auth::user()->id;

        /** Menggunakan relasi polymorphism */
        // $thread->comments()->save($comment);


        /**
         * addComment() didapat dari CommentableTrait yang dideklarasikan di model Thread
         */
        $thread->addComment($request->body);

        return back()->with('msg-success', 'Success Comment');
    }


    /**
     * Untuk reply pada comment
     * Class yang ikut berperan Comment berelasi dengan dirinya sendiri
     */
    public function addReplyComment(Request $request, Comment $comment)
    {
        $this->validate($request, [
            'reply' => 'required',
        ]);

        // $comment = Comment::findOrFail($id);

        /**
         * [MOVE] ke CommentableTrait
         */
        // $reply = new Comment;
        // $reply->body = $request->reply;
        // $reply->user_id = Auth::user()->id;

        /** Menggunakan relasi polymorphism */
        // $comment->comments()->save($reply);

        /**
         * addComment() didapat dari CommentableTrait yang dideklarasikan di model Comment
         */
        $comment->addComment($request->reply);

        return back()->with('msg-success', 'Success Reply');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     /**
      * Comment dan Reply sama-sama menggunakan function update
      */
    public function update(Request $request, $id)
    {
        $comment = Comment::findOrFail($id);

        /**
         * Jika bukan comment yang punya maka akan di redirect ke 401
         */
        if($comment->user_id != Auth::user()->id)
            return abort('401');

        $comment->update([
            'body' => $request->body,
        ]);

        return back()->with('msg-success', 'Comment Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
      * Comment dan Reply sama-sama menggunakan function destroy
      */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);

        /**
         * Jika bukan comment yang punya maka akan di redirect ke 401
         */
        if($comment->user_id != Auth::user()->id)
            return abort('401');

        $comment->delete();

        return back()->with('msg-delete', 'Comment Deleted');
    }
}

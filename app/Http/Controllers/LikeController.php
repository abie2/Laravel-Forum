<?php

namespace App\Http\Controllers;

use Auth;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class LikeController extends Controller
{
    public function toggleLike()
    {
        $commentId = Input::get('commentId');
        $comment = Comment::findOrFail($commentId);

        $usersLike = $comment->likes()
            ->where('user_id', Auth::user()->id)
            ->where('likeable_id', $commentId)->first();
        if (!$usersLike) {
            // likeIt dari Likeable trait
            $comment->likeIt();
            return response()->json(['status' => 'success', 'message' => 'liked']);
        } else {
            $comment->unlikeIt($usersLike->id);
            return response()->json(['status' => 'success', 'message' => 'unliked']);
        }
    }

}

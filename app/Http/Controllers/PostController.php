<?php

namespace App\Http\Controllers;

use File;
use Auth;
use App\Post;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);
        try {
            $user_id = Auth::user()->id;
            $title = $request->title;
            /** Validasi agar slug tidak sama */
            $slug = str_slug($title, '-');
            if (Post::where('slug', $slug)->first() != null)
                $slug = $slug . '-' . time();
            $short_description = $request->short_description;
            $description = $request->description;
            $published = true;

            $save_image = null;
            /** Insert Image */
            if ($request->hasFile('image')) {
                $save_image = $this->saveImage($title, $request->file('image'));
            }
            $image = $filename;

            $post = Post::create([
                'user_id' => $user_id,
                'title' => $title,
                'slug' => $slug,
                'short_description' => $short_description,
                'description' => $description,
                'image' => $image,
                'published' => $published
            ]);

            return redirect()->route('admin.post')
                ->with('msg-success', 'Post Created Success');
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.show');
    }

    public function show1() {
        return view('posts.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        try {
            $post = Post::where('slug', $slug)->first();
            return view('posts.edit', compact('post'));
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $post = Post::findOrFail($id);

            $user_id = Auth::user()->id;
            $title = $request->title;
            /** Validasi agar slug tidak sama */
            $slug = str_slug($title, '-');
            if (Post::where('slug', $slug)->first() != null)
                $slug = $slug . '-' . time();
            $short_description = $request->short_description;
            $description = $request->description;
            $published = true;

            $save_image = null;
            /** Insert Image */
            if ($request->hasFile('image')) {
                $save_image = $this->saveImage($title, $request->file('image'));
            }
            $image = $filename;
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $post = Post::findOrFail($id);
            if (!empty($post->image))
                File::delete(public_path('images/posts/' . $post->image));
            $post->delete();

            return redirect()->route('product.index')->with($alert);
        } catch (\Exception $e) {
            return redirect()->back()
                ->with(['error' => $e->getMessage()]);
        }
    }

    private function saveImage($name, $photo)
    {
        /** Buat nama file */
        $image_name = $name . uniqid() . '.' . $photo->getClientOriginalExtension();

        /** Buat tempat penyimpanan gambar */
        $path = public_path('images/posts');

        /** Cek sudah ada directory */
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        /** Simpan gambar */
        Image::make($photo)->save($path . '/' . $image_name);

        return $image_name;
    }

}

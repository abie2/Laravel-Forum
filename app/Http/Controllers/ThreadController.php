<?php

namespace App\Http\Controllers;

use Auth;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ThreadController extends Controller
{
    /**
     * Create a new controller instance.
     * Untuk setiap controller yang harus terautentikasi gunakan function contsruct
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function markAsSolution()
    {
        // dd(Input::all());

        /**
         * Setiap Thread punya 1 buah solution,
         * dimana ketika jawaban sudah ditandai sebagai solution,
         * maka kolom solution pada table thread akan diisi dengan comment_id
         * yang menjadi solusi pada thread tersebut.
         */
        $threadId = Input::get('threadId');
        $solutionId = Input::get('solutionId');

        $thread = Thread::findOrFail($threadId);

        // melakukan update pada table thread
        $thread->solution = $solutionId;

        if ($thread->save()) {
            if (request()->ajax()) {
                return response()->json(['status' => 'success', 'message' => 'marked as solution']);
            }

            return back()->with('msg-success', 'Solution Marked');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $threads = Thread::paginate(15);

        // $id = auth()->user()->id; ambil id yang login
        // $id = Auth::user()->id;

        return view('threads.index', compact('threads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('threads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required',
            'thread' => 'required',
            'type' => 'required',
        ]);

        Thread::create([
            'subject' => $request->subject,
            'thread' => $request->thread,
            'type' => $request->type,
            'user_id' => Auth::user()->id,
        ]);

        return redirect()->route('thread.index')->with('msg-success', 'Thread Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $thread = Thread::findOrFail($id);

        return view('threads.detail', compact('thread'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $thread = Thread::findOrFail($id);

        return view('threads.edit', compact('thread'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $thread = Thread::findOrFail($id);

        if(Auth::user()->id != $thread->user_id) {
            return abort(401, 'Unathorized');
        }

        $thread->update([
            'subject' => $request->subject,
            'thread' => $request->thread,
            'type' => $request->type,
        ]);

        return redirect()->route('thread.index')->with('msg-success', 'Thread Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $thread = Thread::findOrFail($id);

        if(Auth::user()->id != $thread->user_id) {
            return abort(401, 'Unathorized');
        }

        $thread->delete();

        return redirect()->route('thread.index')->with('msg-delete', 'Thread Deleted');
    }
}

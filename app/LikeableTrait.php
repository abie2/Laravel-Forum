<?php

namespace App;

use Auth;

trait LikeableTrait
{
    public function likes()
    {
        return $this->morphMany('App\Like', 'likeable')->latest();
    }

    public function unlikeIt()
    {
        // $like = Like::find($id);
        $like = $this->likes()->where('user_id', Auth::user()->id)->delete();

        return true;
    }

    /**
     * function likeIt untuk like
     */
    public function likeIt()
    {
        $like = new Like();
        $like->user_id = Auth::user()->id;

        $this->likes()->save($like);

        return $like;
    }

    public function isLiked()
    {
        return !!$this->likes()->where('user_id', Auth::user()->id)->count();
    }

}
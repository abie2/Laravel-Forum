<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";

    protected $fillable = [
        'title', 'user_id', 'slug', 'short_description',
        'description', 'image', 'published',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}

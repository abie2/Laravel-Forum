<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use CommentableTrait;

    protected $guarded = [];

    protected $fillable = [
        'subject', 'thread', 'type', 'user_id'
    ];

    /**
     * sebuah thread dimiliki oleh seorang user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * [MOVE] ke CommentableTrait
     */
    // public function comments()
    // {
    //     return $this->morphMany('App\Comment', 'commentable')->latest();
    // }
}

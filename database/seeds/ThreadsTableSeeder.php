<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
class ThreadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        $threads = [
            [
                'subject' => 'Laravel',
                'thread' => 'This is Laravel',
                'type' => 'question',
                'user_id' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'subject' => 'Android Studio',
                'thread' => 'This is Android Studio',
                'type' => 'question',
                'user_id' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'subject' => 'Artificial Intelligence',
                'thread' => 'This is AI',
                'type' => 'question',
                'user_id' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],


        ];

        DB::table('threads')->insert($threads);
    }
}

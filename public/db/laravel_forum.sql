-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 01, 2018 at 10:25 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_forum`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentable_id` int(11) NOT NULL,
  `commentable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `body`, `commentable_id`, `commentable_type`, `created_at`, `updated_at`) VALUES
(8, 1, 'Cobain belahjar', 2, 'App\\Thread', '2018-07-25 22:48:35', '2018-07-25 22:48:35'),
(10, 1, 'Remember boy :) yyy', 9, 'App\\Thread', '2018-07-25 23:55:33', '2018-07-26 00:14:15'),
(11, 1, 'muntabbbs . . .', 9, 'App\\Thread', '2018-07-26 00:14:29', '2018-07-26 00:14:29'),
(12, 2, 'Cuanggih euy', 9, 'App\\Thread', '2018-07-26 00:21:06', '2018-07-26 00:21:06'),
(14, 2, 'ini reply aja ok', 13, 'App\\Comment', '2018-07-26 01:33:50', '2018-07-26 01:44:10'),
(15, 2, 'dsadf', 13, 'App\\Comment', '2018-07-26 01:34:02', '2018-07-26 01:34:02'),
(16, 2, 'ok reply dah jalan', 13, 'App\\Comment', '2018-07-26 01:35:57', '2018-07-26 01:35:57'),
(17, 2, '111ok reply dah jalan', 13, 'App\\Comment', '2018-07-26 01:36:11', '2018-07-26 01:44:33'),
(18, 2, 'iydong', 11, 'App\\Comment', '2018-07-26 01:36:27', '2018-07-26 01:36:27'),
(19, 2, 'next to do it ....', 12, 'App\\Comment', '2018-07-26 02:04:38', '2018-07-26 02:04:38'),
(21, 2, 'Just do it', 12, 'App\\Comment', '2018-07-26 02:56:31', '2018-07-26 02:56:31'),
(24, 1, 'test 123', 9, 'App\\Thread', '2018-07-26 18:42:23', '2018-07-26 18:42:23'),
(27, 1, 'dfdsafdsadfs 123', 9, 'App\\Thread', '2018-07-26 18:49:30', '2018-07-26 18:49:30'),
(28, 1, 'fdas', 27, 'App\\Comment', '2018-07-26 18:53:57', '2018-07-26 18:53:57'),
(29, 1, 'Masuk', 9, 'App\\Thread', '2018-07-31 23:39:53', '2018-07-31 23:39:53'),
(30, 1, 'ok', 29, 'App\\Comment', '2018-07-31 23:40:02', '2018-07-31 23:40:02'),
(33, 1, 'mantap', 1, 'App\\Thread', '2018-08-01 02:25:24', '2018-08-01 02:25:24'),
(34, 1, 'ok mantap', 33, 'App\\Comment', '2018-08-01 02:25:59', '2018-08-01 02:25:59'),
(35, 1, 'What you need is a \"has many through many\" relationship but laravel doesn\'t support it.\r\n\r\nHere is an old thread about it. http://laravel.io/forum/03-04-2014-hasmanythrough-with-many-to-many\r\n\r\nThere are some workarounds people have come up with and I can offer you another one which I think is pretty clean. It\'s a scope that has nested whereHas inside the model C\r\n\r\nmodel C --\r\n\r\npublic function scopeHasAViaB($query, $ids) {\r\n    if (empty($ids)) {\r\n        return new \\Illuminate\\Database\\Eloquent\\Collection();\r\n    }\r\n    return $query->whereHas(\'b\', function ($query) use ($ids) {\r\n            $query->whereHas(\'a\', function ($query) use ($ids) {\r\n                $query->whereIn(\'id\', $ids);\r\n            });\r\n        });\r\n}\r\n-- controller code --\r\n\r\n  $modelAkeys = [1,2]\r\n    $cc = ModelC::HasAViaB($modelAKeys)->paginate(10);', 10, 'App\\Thread', '2018-08-01 03:14:12', '2018-08-01 03:14:12'),
(36, 1, 'Thank you, It works like a charm, but it takes a lot of time in case of ~5 mil C rows.', 35, 'App\\Comment', '2018-08-01 03:14:38', '2018-08-01 03:14:38'),
(37, 1, 'I created a HasManyThrough relationship with support for BelongsToMany: Repository on GitHub\r\n\r\nAfter the installation, you can use it like this:\r\n\r\nclass A extends Model {\r\n    use \\Staudenmeir\\EloquentHasManyDeep\\HasRelationships;\r\n\r\n    public function c() {\r\n        return $this->hasManyDeep(C::class, [\'a_b\', B::class, \'b_c\']);\r\n    }\r\n}', 10, 'App\\Thread', '2018-08-01 03:17:12', '2018-08-01 03:17:12'),
(38, 4, 'You can clone the project to the server (suspecting to have it in git), configure the .env file. Do a composer install and artisan migrate. Point the document root to the public directory and do any other step needed for your application.\r\n\r\nBut if you need a full configuration I honestly think it is the best to contact a server admin. Because it will depend on the existing configuration how it need to be done.', 11, 'App\\Thread', '2018-08-01 03:20:05', '2018-08-01 03:20:05'),
(39, 4, 'It looks like PHP is not installed or enabled for the site.', 11, 'App\\Thread', '2018-08-01 03:20:36', '2018-08-01 03:20:36'),
(40, 4, 'Ooops!!!!!!!!\r\n\r\n@Tobias van Beek Thanks.', 38, 'App\\Comment', '2018-08-01 03:20:49', '2018-08-01 03:20:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_25_062850_create_threads_table', 1),
(4, '2018_07_26_040440_create_comments_table', 2),
(5, '2018_07_27_020736_add_solution_to_threads_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE `threads` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thread` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `solution` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`id`, `subject`, `thread`, `type`, `solution`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Laravel', 'This is Laravel', 'question', 33, 2, '2018-07-25 00:04:15', '2018-08-01 02:25:30'),
(2, 'Android Studio', 'This is Android Studio', 'question', NULL, 1, '2018-07-25 00:04:15', '2018-07-25 00:04:15'),
(3, 'Artificial Intelligence 1', 'This is AI', 'question', NULL, 1, '2018-07-25 00:04:15', '2018-07-25 01:26:02'),
(6, 'Access Constructor from Method', 'Hello GUYS , I am trying to access those values of the controller , but I have to make variables first , I wold like to pass those values direct.', 'Question', NULL, 1, '2018-07-25 01:40:05', '2018-07-25 01:40:05'),
(9, 'test markdown', '[Cara Menggunakan Markdown](https://laravel.io/forum/01-31-2014-how-to-mark-up-forum-posts)\r\n# heading 1\r\n## heading 2\r\n### heading 3\r\n#### heading 4\r\n##### heading 5\r\n\r\nini adalah kalimat pertama untuk line-breaks spasi 2x.  \r\ndan ini kalimat kedua setelah line-breaks.\r\n\r\nok\r\n\r\n\r\nIni paragraf kedua untuk paragraf baru enter 2x.\r\n\r\n\r\n*Ini tulisan miring (italic)*  \r\n**Ini tulisan tebal (bold)**  \r\n***Ini tulisan miring (italic) dan tebal (bold)  ***   \r\nIni `code block`  \r\n\r\n\r\n- list 1\r\n- list 2\r\n- list 3\r\n\r\n\r\n    public function image() {\r\n        return view(\'spasi 4 x buat code block\');\r\n    }', 'markdown', 12, 1, '2018-07-25 02:52:58', '2018-08-01 02:24:30'),
(10, 'Connect 3 tables with 2 pivot tables', '\'m trying to find all rows in c table (see DB schema) associated with one row in a table. Connected by 2 pivot tables each with n:m relationship.\r\n\r\nDB Schema\r\n\r\nHere is how I defined the relationships:\r\n\r\nIn model A: \r\n  $this->belongsToMany(\"App\\B\");\r\n\r\nIn Model B:\r\n  $this->belongsToMany(\"App\\A\");\r\n  $this->belongsToMany(\"App\\C\");\r\n\r\nIn model C:\r\n  $this->belongsToMany(\"App\\B\");\r\nI\'ve already reached the goal by addding the method getC into A Eloquent model:\r\n\r\npublic function getC() {\r\n    $cCollection = new Collection();\r\n\r\n    $this->b()->get()->each(function($b) use (&$cCollection){\r\n        $b->c()->get()->each(function($c) use (&$cCollection) {\r\n            $cCollection->add($c);\r\n        });\r\n    });\r\n\r\n    return $cCollection;\r\n}\r\nAnd it works perfectly, but it doesn\'t fit my requirements. If i build a new collection i\'m unable to do pagination, etc... on rows from C table. It would be also pretty heavy in case of thousand of rows.\r\n\r\nIf i try to do something like this:\r\n\r\nA::findOrFail($id)->b\r\ni get a Collection of B but i\'m unable to directly get all C. If i iterate the collection of B:\r\n\r\nA::findOrFail($id)->b->each(function($b){\r\n    $b->c();\r\n});\r\nI get all C but I lose direct access to C Eloquent and i\'m unable to do count, paginate, etc.. on it.\r\n\r\nIs there any other way how to do something like this?\r\n\r\nA::findOrFail($id)->b->c->paginate(n);\r\nA::findOrFail($id)->b->c->count();\r\nA::findOrFail($id)->b->c->all();\r\netc...', 'Database', 35, 1, '2018-08-01 03:13:48', '2018-08-01 03:14:50'),
(11, 'Run laravel project on existing apache server of centos', 'I have a server of CentOs 7 where apache is running. I\'m using it for run my web applications. Now I want to run a laravel 5.0 project on it. I know its weird but reality is server php version is so old 5.4 and running other applications.\r\n\r\nNow I need to deploy it to my production server and run as https://server-ip/laravel-project\r\n\r\nNeed full configuration.', 'Instalation', 38, 3, '2018-08-01 03:19:10', '2018-08-01 03:20:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'superadmin@gmail.com', '$2y$10$WjHStRFX2qgRF/5v0.yQle0UFBxIrQMTjs8eUFSNWJkES9CwukO0a', 'RtJXm9e4Z7mVZsUiusJrJ6iLXgR3RKhdLHrt5XGLY3aegi62niWLCiQCHfou', '2018-07-25 01:44:04', '2018-07-25 01:44:04'),
(2, 'Nando', 'nando@gmail.com', '$2y$10$ZGAbH0gGZO6U0Gu2ngN6yuFvaZkVkN3B6pbB6FiuLyJcoyEMB1FIe', NULL, '2018-07-26 00:20:39', '2018-07-26 00:20:39'),
(3, 'Mr. Smith', 'smith@gmail.com', '$2y$10$ldU8sBpaDX6y0QDR/cqUqum2gw0rGf6y8Cgc4q15WBU11S9MOUyR.', 'o60lMruQV8DZWIE41bFRNbjLjpXp8UNkKzONjEEaX3FnVSvWxyOPF0ZlDvsE', '2018-08-01 03:18:30', '2018-08-01 03:18:30'),
(4, 'Tobias Van Dreek', 'tobias@gmail.com', '$2y$10$Ke3ginAM4M5EiP1kwvRPYOw/TlTlXhwvObNte7M/fFBG/Kreau0.u', NULL, '2018-08-01 03:19:50', '2018-08-01 03:19:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

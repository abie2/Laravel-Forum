## Tampilan
1. https://bootswatch.com/cyborg/
2. template comment: https://bootsnipp.com/snippets/M5obX

## Library
1. Markdown: https://github.com/michelf/php-markdown
   Cara penggunaan: https://laravel.io/forum/01-31-2014-how-to-mark-up-forum-posts
2. Captcha: https://github.com/anhskohbo/no-captcha
3. Laravolt/avatar: https://github.com/laravolt/avatar
4. https://github.com/laravolt/indonesia

## Cara
1. ganti icon prev dan next di slick slider: https://stackoverflow.com/questions/27403501/how-to-style-prev-next-arrows-button

## Comment System
https://laravel.com/docs/5.6/eloquent-relationships#polymorphic-relations

## To Do
1. Orang yang boleh memilih mark as solution adalah orang yang membuat thread.

## Task
1. Membuat blog terdiri dari post dan komentar
2. halaman awal dimana yang tidak login bisa melihat post tapi tidak bisa memberi komentar
3. Membuat create postingan oleh admin/ahli
4. Memperbaiki navbar untuk daftar dan masuk
5. Menjalankan fungsi controller post

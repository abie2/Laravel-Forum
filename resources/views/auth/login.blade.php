@extends('layouts.main-login')
@section('title', 'Masuk')

@section('top-resource')
@endsection

@section('content')
    <div class="container" style="margin-top:7rem; min-height: 518px;">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card mb-3">
                    <div class="card-header">
                        MASUK
                    </div>
                    <div class="card-body">
                        <form action="{{ route('login') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">Alamat E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" 
                                 value="{{ old('email') }}" required autofocus style="border: 1px solid #ecf0f1">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password">Kata Sandi</label>
                                <input type="password" class="form-control" id="password" name="password" 
                                required style="border: 1px solid #ecf0f1">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Masuk</button>
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Lupa Kata Sandi?
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('bottom-resource')

@endsection
@extends('layouts.main')
@section('title', 'Beranda')

@section('top-resource')
<style>
    .pilihan-editor {
        font-size: 2rem;
    }

    .card-editor {
        padding: 1rem;
    }
    .icon-num {
        font-size: 2.5rem;
        font-weight: bold;
    }
    .question-editor {
        height: 90%;
        margin-top: 5%;
        margin-bottom: 5%;
        font-size: 1.1rem;
        font-weight: 500;
    }
    .card-banner {
        padding-top: 1.5rem;
        padding-bottom: 1.5rem;
        padding-left: 3.3rem;
        padding-right: 3.3rem;
    }
    /* .card-banner img {
        width: 620px;
    } */
    .banner-title {
        margin-top: 1rem;
        font-size: 1.5rem;
    }
    .banner-description {
        font-size: 0.9rem;
        margin-bottom: 1rem;
    }
</style>
@endsection

@section('content')
<div class="container" style="margin-top: 7rem;">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-8">
            <section class="regular slider">
                <div>
                    <div class="card card-banner">
                        <img class="img-fluid" src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                        <h2 class="banner-title"><a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a></h2>
                        <div class="banner-description">
                            Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis.
                        </div>
                        <a href="#">Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
                <div>
                    <div class="card card-banner">
                        <img class="img-fluid" src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                        <h2 class="banner-title"><a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a></h2>
                        <div class="banner-description">
                            Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis.
                        </div>
                        <a href="#">Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
                <div>
                    <div class="card card-banner">
                        <img class="img-fluid" src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                        <h2 class="banner-title"><a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a></h2>
                        <div class="banner-description">
                            Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis.
                        </div>
                        <a href="#">Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
                <div>
                    <div class="card card-banner">
                        <img class="img-fluid" src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                        <h2 class="banner-title"><a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a></h2>
                        <div class="banner-description">
                            Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis.
                        </div>
                        <a href="#">Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-4">
            {{-- <h2>Pilihan Editor</h2> --}}
            <div class="card card-editor mb-3">
                {{-- <div class="card-editor"> --}}
                    <h2 class="pilihan-editor">Pilihan Editor</h2>
                    <hr class="style-editor">
                    <div class="description">
                        <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2">
                                <span class="icon-num">1</span>
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10">
                                <a href="#" class="question-editor">3 Cara Mudah Membersihkan Lensa Kacamata</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2">
                                <span class="icon-num">2</span>
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10">
                                <a href="#" class="question-editor">3 Cara Mudah Membersihkan Lensa Kacamata</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2">
                                <span class="icon-num">3</span>
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10">
                                <a href="#" class="question-editor">3 Cara Mudah Membersihkan Lensa Kacamata</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2">
                                <span class="icon-num">4</span>
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10">
                                <a href="#" class="question-editor">3 Cara Mudah Membersihkan Lensa Kacamata</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-2 col-md-2 col-lg-2">
                                <span class="icon-num">5</span>
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10">
                                <a href="#" class="question-editor">3 Cara Mudah Membersihkan Lensa Kacamata</a>
                            </div>
                        </div>
                    </div>
                {{-- </div> --}}
            </div>
        </div>
    </div>
    <hr>
</div>
@endsection

@section('bottom-resource')
<script>
   $(".regular").slick({
        dots: true,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
      });
</script>
@endsection
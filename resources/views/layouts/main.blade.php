<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CEKSEHATKU | @yield('title')</title>

    <!-- Styles -->
    @include('partials.css-plug')
    @yield('top-resource')
</head>
<body>
    <div id="app">
        @include('partials.navbar')
        @yield('content')
        @include('partials.footer')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @include('partials.js-plug')
    @yield('bottom-resource')
</body>
</html>

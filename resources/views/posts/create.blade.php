@extends('layouts.main')
@section('title', 'Buat Post')

@section('top-resource')
<style>
.sejenis {
    width: 100px;
}
</style>
@endsection

@section('content')
    <div class="container" style="margin-top:7rem;">
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-3">
                    <div class="card-header">
                        Baca Artikel
                    </div>
                    <div class="card-body" style="padding: 0px;">
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" 
                                        class="sejenis" alt="Manfaat Aroma Terapi">
                                    </div>
                                    <div class="col-md-8">
                                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" 
                                        class="sejenis" alt="Manfaat Aroma Terapi">
                                    </div>
                                    <div class="col-md-8">
                                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" 
                                        class="sejenis" alt="Manfaat Aroma Terapi">
                                    </div>
                                    <div class="col-md-8">
                                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" 
                                        class="sejenis" alt="Manfaat Aroma Terapi">
                                    </div>
                                    <div class="col-md-8">
                                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" 
                                        class="sejenis" alt="Manfaat Aroma Terapi">
                                    </div>
                                    <div class="col-md-8">
                                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-header">
                        BUAT POST
                    </div>
                    <div class="card-body">
                        <form action="{{ route('post.store') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="title">Judul</label>
                                <input type="text" class="form-control" name="title" 
                                placeholder="{{ old('title') }}" style="border: 1px solid #ecf0f1">
                            </div>

                            <div class="form-group">
                                <label for="short_description">Deskripsi Singkat</label>
                                <textarea class="form-control" name="short_description" id="short_description" 
                                cols="30" rows="3" style="border: 1px solid #ecf0f1">{{ old('short_description') }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="description">Deskripsi</label>
                                <textarea class="form-control my-editor" id="description" name="description" 
                                cols="30" rows="10" style="border: 1px solid #ecf0f1">{!! old('description') !!}</textarea>
                                {{-- <script src="{{ asset('vendor/tinymce_4.8.0/tinymce/js/tinymce/tinymce.min.js') }}"></script> --}}
                            </div>

                            <div class="form-group">
                                <label for="image" class="col-md-4 control-label">Gambar</label>
                                {{-- <img width="820" height="481" /> --}}
                                <input type="file" class="uploads form-control" style="margin-top: 20px; border: 1px solid #ecf0f1" 
                                name="image" value="{{ old('image') }}">
                            </div>

                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Buat</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('bottom-resource')

@endsection

@extends('layouts.main')
@section('title', 'Edit Post')

@section('top-resource')
@endsection

@section('content')
    <div class="container" style="margin-top:7rem;">
        <div class="row">
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Laravel
                        <span class="badge badge-primary badge-pill">14</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Blade
                        <span class="badge badge-primary badge-pill">2</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Cache
                        <span class="badge badge-primary badge-pill">1</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Database
                        <span class="badge badge-primary badge-pill">1</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Lumen
                        <span class="badge badge-primary badge-pill">1</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-header">
                        EDIT POST
                    </div>
                    <div class="card-body">
                        <form action="{{ route('post.store') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="title">Judul</label>
                                <input type="text" class="form-control" name="title" 
                                value="{{ $product->title }}" style="border: 1px solid #ecf0f1">
                            </div>

                            <div class="form-group">
                                <label for="short_description">Deskripsi Singkat</label>
                                <textarea class="form-control" name="short_description" id="short_description" 
                                cols="30" rows="3" style="border: 1px solid #ecf0f1" value="{{ $product->short_description }}"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="description">Deskripsi</label>
                                <textarea class="form-control my-editor" id="description" name="description" 
                                cols="30" rows="10" style="border: 1px solid #ecf0f1" value="description">{!! old('description') !!}</textarea>
                                {{-- <script src="{{ asset('vendor/tinymce_4.8.0/tinymce/js/tinymce/tinymce.min.js') }}"></script> --}}
                            </div>

                            <div class="form-group">
                                <label for="image" class="col-md-4 control-label">Gambar</label>
                                <input type="file" class="uploads form-control" style="margin-top: 20px; border: 1px solid #ecf0f1" 
                                name="image" value="{{ old('image') }}">
                            </div>

                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan Perubahan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('bottom-resource')

@endsection

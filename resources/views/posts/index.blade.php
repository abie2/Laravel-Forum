@extends('layouts.main')
@section('title', 'Semua Post')

@section('top-resource')
<style>
</style>
@endsection

@section('content')
<div class="container post" style="margin-top:7rem;">
    <h1 class="article">Artikel Kesehatan</h1>
    <hr class="style-eight">
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card mb-3">
                <div class="card-header">
                    <div class="title">
                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                    </div>
                </div>
                <div class="card-body">
                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                    <div class="author">
                        Ditulis oleh: <a href="#" title="penulis">Dr. Hendra</a>
                    </div>
                    <div class="description">
                        <p>Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis.</p>
                        <a href="#">Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card mb-3">
                <div class="card-header">
                    <div class="title">
                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                    </div>
                </div>
                <div class="card-body">
                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                    <div class="author">
                        Ditulis oleh: <a href="#" title="penulis">Dr. Hendra</a>
                    </div>
                    <div class="description">
                        <p>Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis.</p>
                        <a href="#">Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card mb-3">
                <div class="card-header">
                    <div class="title">
                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                    </div>
                </div>
                <div class="card-body">
                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                    <div class="author">
                        Ditulis oleh: <a href="#" title="penulis">Dr. Hendra</a>
                    </div>
                    <div class="description">
                        <p>Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis.</p>
                        <a href="#">Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card mb-3">
                <div class="card-header">
                    <div class="title">
                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                    </div>
                </div>
                <div class="card-body">
                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                    <div class="author">
                        Ditulis oleh: <a href="#" title="penulis">Dr. Hendra</a>
                    </div>
                    <div class="description">
                        <p>Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis.</p>
                        <a href="#">Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card mb-3">
                <div class="card-header">
                    <div class="title">
                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                    </div>
                </div>
                <div class="card-body">
                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                    <div class="author">
                        Ditulis oleh: <a href="#" title="penulis">Dr. Hendra</a>
                    </div>
                    <div class="description">
                        <p>Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis.</p>
                        <a href="#">Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card mb-3">
                <div class="card-header">
                    <div class="title">
                        <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                    </div>
                </div>
                <div class="card-body">
                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                    <div class="author">
                        Ditulis oleh: <a href="#" title="penulis">Dr. Hendra</a>
                    </div>
                    <div class="description">
                        <p>Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis.</p>
                        <a href="#">Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href="#">Sebelum</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">Selanjutnya</a></li>
        </ul>
    </nav>
</div>
@endsection

@section('bottom-resource')

@endsection
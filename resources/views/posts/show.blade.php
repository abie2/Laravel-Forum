@extends('layouts.main')
@section('title', 'Rincian Post')

@section('top-resource')
<style>
</style>
@endsection

@section('content')
<div class="container detail-post" style="margin-top: 7rem;">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8">
            <div class="card mb-3">
                <div class="card-header">
                    <div class="title">
                        <h1>Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</h1>
                    </div>
                </div>
                <div class="card-body">
                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                    <div class="author">
                        Ditulis oleh: <a href="#" title="penulis">Dr. Hendra 28 Oktober 2018</a>
                    </div>
                    <div class="description">
                        <p>Saat ini, aromaterapi banyak digunakan sebagai perawatan alternatif untuk berbagai kondisi kesehatan. Mulai dari stres, masalah pencernaan, insomnia, depresi, dan lain-lain. Tak heran kalau semakin lama semakin mudah buat Anda untuk menemukan berbagai produk aromaterapi yang katanya berkhasiat bagi tubuh. Apakah benar ada banyak manfaat aromaterapi buat Anda? Cari tahu jawabannya di bawah ini.</p>
                        <p>Sebenarnya, apa yang dimaksud dengan aromaterapi?
                            Aromaterapi adalah cara memanfaatkan minyak alami yang diekstrak dari tumbuhan dengan tujuan meningkatkan kesehatan secara fisik maupun psikis. Minyak yang digunakan adalah minyak esensial yang terbuat dari berbagai tanaman obat, bunga, herbal, akar, buah, dan pepohonan yang tumbuh di seluruh dunia. Menurut sejumlah penelitian, beberapa jenis minyak esensial sudah terbukti memiliki efek positif untuk meningkatkan kondisi fisik dan emosional seseorang.
                            Sudah lebih dari 5000 tahun, aromaterapi dipercaya diantara berbagai budaya di seluruh dunia. Dipercaya bahwa aromaterapi merupakan penyembuh alami yang dapat berfungsi sebagai anti bakteri, antiradanga, sekaligus memberikan efek antinyeri.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="card mb-3">
                <div class="card-header subscribe">
                    Berlangganan Artikel
                    {{-- <p>Masukkan email untuk berlangganan</p> --}}
                    <form action="">
                        <input type="text" class="form-control" placeholder="Masukkan email untuk berlangganan">
                        <button class="btn btn-primary">Kirim <i class="fa fa-send"></i></button>
                    </form>
                </div>
            </div>

            <div class="card mb-3">
                <div class="card-header">
                    Artikel Sejenis
                </div>
                <div class="card-body" style="padding: 0px;">
                    <ul class="list-group">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                                </div>
                                <div class="col-md-8">
                                    <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                                </div>
                                <div class="col-md-8">
                                    <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                                </div>
                                <div class="col-md-8">
                                    <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                                </div>
                                <div class="col-md-8">
                                    <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{ asset('images/posts/manfaat-aromaterapi.jpg') }}" alt="Manfaat Aroma Terapi">
                                </div>
                                <div class="col-md-8">
                                    <a href="#">Apa Saja Manfaat Aromaterapi Bagi Kesehatan Tubuh?</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('bottom-resource')

@endsection
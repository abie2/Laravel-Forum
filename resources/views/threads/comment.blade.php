<div class="container" style="margin-bottom:2rem;">
	<h4 class="text-center">Comments:</h4>

    @forelse($thread->comments as $comment)
        <div class="card mb-3">
            <div class="card-header">
                <div class="profile">
                    <a href="https://laravel.io/user/sidney%20machara">
                        <img class="rounded-circle img-fluid" style="width:2rem;" src="https://www.gravatar.com/avatar/09827822f952b6b0e2bbbbe182618d3f?s=25&amp;d=https%3A%2F%2Flaravel.io%2Favatar%2Fsidney%2520machara">
                    </a>
                    <a href="https://laravel.io/user/sidney%20machara">{{ $comment->user->name }}</a>
                    posted {{ $comment->created_at }}
                    @if(!empty($thread->solution))
                        @if($thread->solution == $comment->id)
                            <span class="btn btn-success pull-right">Solution</span>
                        @endif
                    @endif
                </div>
            </div>
            <div class="card-body">
                <h5>{{ $comment->body }}</h5>
                <br>
                @if(auth()->check())
                    @if(Auth::user()->id == $comment->user_id)
                        <!-- Button trigger modal -->
                        <a href="#{{ $comment->id }}" type="button" class="btn btn-primary" data-toggle="modal">
                            <i class="fa fa-edit"></i> Edit
                        </a>
                        <form action="{{ route('comment.destroy', $comment->id) }}" method="POST" style="display:inline;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button class="btn btn-danger" type="submit"><i class="fa fa-close"></i> Delete</button>
                        </form>
                        <!-- Modal -->
                        <div class="modal fade" id="{{ $comment->id }}">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Comment</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('comment.update', $comment->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="body">Comment</label>
                                            <input type="text" class="form-control" id="body" name="body" value="{{ $comment->body ? $comment->body : old('body') }}" placeholder="{{ $comment->body }}">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Edit</button>
                                </form>
                                </div>
                            </div>
                            </div>
                        </div>
                    @endif
                @endif
                <a class="float-right btn btn-outline-primary ml-2" onclick="toggleReply('{{ $comment->id }}')" id="reply"> <i class="fa fa-reply"></i> Reply</a>
                @if(empty($thread->solution))
                    <div class="btn btn-success float-right" onclick="markAsSolution('{{ $thread->id }}', '{{ $comment->id }}', this)" style="margin-bottom: 3px;"><i class="fa fa-check"></i> Mark As Solution</div>
                @endif
                <div class="reply-form-{{ $comment->id }} d-none">
                    <h5 class="text-center">Reply Here:</h5>
                    <form action="{{ route('replycomment.store', $comment->id) }}" method="POST">
                        {{ csrf_field() }}
                        <textarea class="form-control" name="reply" id="reply" cols="20" rows="3" placeholder="Write for reply . . . . .">{{ old('reply') }}</textarea>
                        <button type="submit" class="btn btn-primary float-right" style="margin-top:1rem;"><i class="fa fa-check"></i> Submit Reply</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        @foreach($comment->comments as $reply)
        <div class="card mb-3 card-inner">
                <div class="card-header">
                    <div class="profile">
                        <a href="https://laravel.io/user/sidney%20machara">
                            <img class="rounded-circle img-fluid" style="width:2rem;" src="https://www.gravatar.com/avatar/09827822f952b6b0e2bbbbe182618d3f?s=25&amp;d=https%3A%2F%2Flaravel.io%2Favatar%2Fsidney%2520machara">
                        </a>
                        <a href="https://laravel.io/user/sidney%20machara">{{ $reply->user->name }}</a>
                        posted {{ $reply->created_at }}
                    </div>
                </div>
                <div class="card-body">
                    <h5>{{ $reply->body }}</h5>
                    <br>
                    @if(Auth::user()->id == $reply->user_id)
                        <!-- Button trigger modal -->
                        <a href="#{{ $reply->id }}" type="button" class="btn btn-primary" data-toggle="modal">
                            <i class="fa fa-edit"></i> Edit
                        </a>

                        {{-- <a class="btn btn-primary" data-toggle="modal" data-target="#form-edit" href="">Edit</a> --}}
                        <form action="{{ route('comment.destroy', $reply->id) }}" method="POST" style="display:inline;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button class="btn btn-danger" type="submit"><i class="fa fa-close"></i> Delete</button>
                        </form>

                        <!-- Modal -->
                        <div class="modal fade" id="{{ $reply->id }}">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Reply</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{ route('comment.update', $reply->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="body">Comment</label>
                                            <input type="text" class="form-control" id="body" name="body" value="{{ $reply->body ? $reply->body : old('body') }}" placeholder="{{ $reply->body }}">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Edit Reply</button>
                                </form>
                                </div>
                            </div>
                            </div>
                        </div>

                    @endif
                </div>
            </div>
        @endforeach
    @empty
        <h5 class="text-center" style="margin-top:2rem; margin-bottom:2rem;">Not have comment . . . . .</h5>
    @endforelse
	{{-- <div class="card">
        @forelse($thread->comments as $comment)
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                        <p class="text-secondary text-center">{{ $comment->created_at }}</p>
                    </div>
                    <div class="col-md-10">
                        <p>
                            <a class="float-left" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>{{ $comment->user->name }}</strong></a>
                            
                        </p>
                        <div class="clearfix"></div>
                        <p>{{ $comment->body }}</p>
                        @if(!empty($thread->solution))
                            @if($thread->solution == $comment->id)
                                <button class="btn btn-success float-right">Solution</button>
                            @endif
                        @else
                          
                            <div class="btn btn-success float-right" onclick="markAsSolution('{{ $thread->id }}', '{{ $comment->id }}', this)" style="margin-bottom: 3px;"><i class="fa fa-check"></i> Mark As Solution</div>
                        @endif
                        <div class="action">
                            @if(auth()->check())
                                @if(Auth::user()->id == $comment->user_id)
                                    <!-- Button trigger modal -->
                                    <a href="#{{ $comment->id }}" type="button" class="btn btn-primary" data-toggle="modal">
                                        <i class="fa fa-edit"></i> Edit
                                    </a>
                                    <form action="{{ route('comment.destroy', $comment->id) }}" method="POST" style="display:inline;">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger" type="submit"><i class="fa fa-close"></i> Delete</button>
                                    </form>
                                    <!-- Modal -->
                                    <div class="modal fade" id="{{ $comment->id }}">
                                        <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Comment</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="{{ route('comment.update', $comment->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="body">Comment</label>
                                                        <input type="text" class="form-control" id="body" name="body" value="{{ $comment->body ? $comment->body : old('body') }}" placeholder="{{ $comment->body }}">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </form>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        </div>
                        <p>
                            <a class="float-right btn btn-outline-primary ml-2" onclick="toggleReply('{{ $comment->id }}')" id="reply"> <i class="fa fa-reply"></i> Reply</a>
                            
                    </div>
                </div>
                <div class="reply-form-{{ $comment->id }} d-none" style="margin-left: 4rem;">
                    <h5 class="text-center">Reply Here:</h5>
                    <form action="{{ route('replycomment.store', $comment->id) }}" method="POST">
                        {{ csrf_field() }}
                        <textarea class="form-control" name="reply" id="reply" cols="20" rows="3" placeholder="Write for reply . . . . .">{{ old('reply') }}</textarea>
                        <button type="submit" class="btn btn-primary float-right" style="margin-top:1rem;"><i class="fa fa-check"></i> Submit Reply</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
                @foreach($comment->comments as $reply)
                    <div class="card card-inner">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                    <p class="text-secondary text-center">{{ $reply->created_at }}</p>
                                </div>
                                <div class="col-md-10">
                                    <p><a href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>{{ $reply->user->name }}</strong></a></p>
                                    <p>{{ $reply->body }}</p>
                                    <div class="action">
                                        @if(Auth::user()->id == $reply->user_id)
                                            <!-- Button trigger modal -->
                                            <a href="#{{ $reply->id }}" type="button" class="btn btn-primary" data-toggle="modal">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>

                                            
                                            <form action="{{ route('comment.destroy', $reply->id) }}" method="POST" style="display:inline;">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger" type="submit"><i class="fa fa-close"></i> Delete</button>
                                            </form>

                                            <!-- Modal -->
                                            <div class="modal fade" id="{{ $reply->id }}">
                                                <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Reply</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{ route('comment.update', $reply->id) }}" method="POST">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PUT') }}
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="body">Comment</label>
                                                                <input type="text" class="form-control" id="body" name="body" value="{{ $reply->body ? $reply->body : old('body') }}" placeholder="{{ $reply->body }}">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Edit Reply</button>
                                                    </form>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>

                                        @endif
                                    </div>
                                    <p>
                                        
                                        <a></a>
                                        
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @empty
            <h5 class="text-center" style="margin-top:2rem; margin-bottom:2rem;">Not have comment . . . . .</h5>
        @endforelse

	</div> --}}
</div>
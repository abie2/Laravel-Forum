@extends('layouts.main')
@section('title', 'Create Thread')

@section('top-resource')
<style>
</style>
@endsection

@section('content')
    <div class="container" style="margin-top:7rem;">
        <div class="row">
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Laravel
                        <span class="badge badge-primary badge-pill">14</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Blade
                        <span class="badge badge-primary badge-pill">2</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Cache
                        <span class="badge badge-primary badge-pill">1</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Database
                        <span class="badge badge-primary badge-pill">1</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Lumen
                        <span class="badge badge-primary badge-pill">1</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-header">
                        CREATE THREAD
                    </div>
                    <div class="card-body">
                        <form action="{{ route('thread.store') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" name="subject" placeholder="Enter subject" placeholder="{{ old('subject') }}" style="border: 1px solid #ecf0f1">
                            </div>
                            <div class="form-group">
                                <label for="type">Type</label>
                                <input type="text" class="form-control" name="type" placeholder="Enter type" placeholder="{{ old('type') }}" style="border: 1px solid #ecf0f1">
                            </div>
                            <div class="form-group">
                                <label for="thread">Thread</label>
                                <textarea class="form-control" name="thread" placeholder="Enter thread" cols="30" rows="10" style="border: 1px solid #ecf0f1">{{ old('thread') }}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Create</button>
                        </form>
                    </div>
                </div>
                {{-- <div class="jumbotron">
                    <h4>Create Thread</h4><br>
                    <form action="{{ route('thread.store') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" name="subject" placeholder="Enter subject" placeholder="{{ old('subject') }}">
                        </div>
                        <div class="form-group">
                            <label for="type">Type</label>
                            <input type="text" class="form-control" name="type" placeholder="Enter type" placeholder="{{ old('type') }}">
                        </div>
                        <div class="form-group">
                            <label for="thread">Thread</label>
                            <textarea class="form-control" name="thread" placeholder="Enter thread" cols="30" rows="10">{{ old('thread') }}</textarea>
                        </div>
                        

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div> --}}
            </div>
        </div>
    </div>
@endsection

@section('bottom-resource')

@endsection

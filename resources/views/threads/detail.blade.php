@extends('layouts.main')
@section('title', 'Detail')

@section('top-resource')
<style>
.card-inner{
    margin-left: 4rem;
}
</style>
@endsection

@section('content')
<div class="container" style="margin-top:7rem;">
    <div class="row forum">
        <div class="col-sm-3 col-md-3 col-lg-3">
            <div class="profile-user-info">
                <a href="https://laravel.io/user/nervous-energy">
                    <img class="img-circle" src="https://www.gravatar.com/avatar/09827822f952b6b0e2bbbbe182618d3f?s=25&amp;d=https%3A%2F%2Flaravel.io%2Favatar%2Fsidney%2520machara">
                </a>
                <h2 class="profile-user-name">{{ $thread->user->name }}</h2>
                <div class="profile-social-icons">
                    <a href="#">
                        <i class="fa fa-github"></i>
                    </a>
                </div>
                <hr>
                <a class="btn btn-primary btn-block" href="https://laravel.io/forum/typeerror-cannot-read-property-channel-of-undefined/subscribe">
                    Subscribe
                </a>
                <a class="btn btn-link btn-block" href="http://laravel-forum.pro/thread">
                    <i class="fa fa-arrow-left"></i> Back
                </a>
            </div>
        </div>
        <div class="col-sm-9 col-md-9 col-lg-9">
            {{-- <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Thread</a></li>
                <li class="breadcrumb-item active">{{ $thread->subject }}</li>
            </ol> --}}
            @if(session('msg-success'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-check"></i> Success!</h5>
                    {{ session('msg-success') }}
                </div>
            @endif
            @if(session('msg-delete'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-ban"></i> Alert!</h5>
                    {{ session('msg-delete') }}
                </div>
            @endif

            <h1>"{{ $thread->subject }}"</h1>
            <hr>

            <div class="card mb-3">
                <div class="card-header">
                    <div class="profile">
                        <a href="https://laravel.io/user/sidney%20machara">
                            <img class="rounded-circle img-fluid" style="width:2rem;" src="https://www.gravatar.com/avatar/09827822f952b6b0e2bbbbe182618d3f?s=25&amp;d=https%3A%2F%2Flaravel.io%2Favatar%2Fsidney%2520machara">
                        </a>
                        <a href="https://laravel.io/user/sidney%20machara">{{ $thread->user->name }}</a>
                        posted 1 month ago

                        <a href="https://laravel.io/forum/tags/laravel">
                            <span class="badge badge-default pull-right">{{ $thread->type }}</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    {{-- <h4 class="card-title">Primary card title</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                    <h4>{{ $thread->subject }}</h4>
                    <h5>{{ $thread->thread }}</h5>
                    <br>
                    @if(Auth::user()->id == $thread->user_id)
                        <a class="btn btn-primary" href="{{ route('thread.edit', $thread->id) }}"><i class="fa fa-edit"></i> Edit</a>
                        <form action="{{ route('thread.destroy', $thread->id) }}" method="POST" style="display:inline;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button class="btn btn-danger" type="submit"><i class="fa fa-close"></i> Delete</button>
                        </form>
                    @endif
                </div>
            </div>
            @include('threads.comment')
            <div class="alert alert-info">
                <p>
                    Please make sure you've read our
                    <a href="https://laravel.io/rules" class="alert-link">Forum Rules</a> before replying to this thread.
                </p>
            </div>

            <div class="card mb-3">
                {{-- <div class="card-header">
                    <h4 class="text-center">Comment Here:</h4>
                </div> --}}
                <div class="card-body">
                    @if(count($errors) > 0)
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fa fa-warning"></i> Alert!</h5>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fa fa-warning"></i> Alert!</h5>
                            {{ session('error') }}
                        </div>
                    @endif
                    <form action="{{ route('threadcomment.store', $thread->id) }}" method="POST">
                        {{ csrf_field() }}
                        <textarea class="form-control" name="body" id="body" cols="30" rows="10" placeholder="Write for comment . . . . .">{{ old('comment') }}</textarea>
                        <button type="submit" class="btn btn-primary btn-block" style="margin-top:1rem;"><i class="fa fa-check"></i> Comment</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('bottom-resource')
<script>
    /**
     * Ketika user klik reply maka textarea untuk reply muncul,
     * jika user klik lagi reply dengan textarea yang sedang aktif,
     * maka textarea akan nonaktif lagi
     */
    function toggleReply(commentId) {
        $('.reply-form-' + commentId).toggleClass('d-none');
    }

    /**
     * Ajax untuk mark up solution
     */
    function markAsSolution(threadId, solutionId, elem) {
        var csrf_token = '{{ csrf_token() }}';
        $.post('{{ route('markAsSolution') }}', {threadId:threadId, solutionId:solutionId, _token:csrf_token}, function(data) {
            $(elem).text('Solution');
        });
    }

    function likeIt(commentId, elem) {
        var csrf_token = '{{ csrf_token() }}';
        $.post('{{ route('toggleLike') }}', {commentId:commentId, _token:csrf_token}, function(data) {
            console.log(data);
        });
    }
</script>
@endsection

@extends('layouts.main')
@section('title', 'Edit Thread')

@section('top-resource')
@endsection

@section('content')
    <div class="container" style="margin-top:8rem;">
        <div class="row">
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Cras justo odio
                        <span class="badge badge-primary badge-pill">14</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Dapibus ac facilisis in
                        <span class="badge badge-primary badge-pill">2</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Morbi leo risus
                        <span class="badge badge-primary badge-pill">1</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="jumbotron">
                    <h4>Edit Thread</h4><br>
                    <form action="{{ route('thread.update', $thread->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" name="subject" value="{{ $thread->subject ? $thread->subject : old('subject') }}" placeholder="{{ old('subject') }}">
                        </div>

                        <div class="form-group">
                            <label for="type">Type</label>
                            <input type="text" class="form-control" name="type" value="{{ $thread->type ? $thread->type : old('type') }}" placeholder="{{ old('type') }}">
                        </div>

                        <div class="form-group">
                            <label for="thread">Thread</label>
                            <textarea class="form-control" name="thread" placeholder="{{ old('thread') }}" cols="30" rows="10">{{ $thread->thread ? $thread->thread : old('thread') }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('bottom-resource')

@endsection

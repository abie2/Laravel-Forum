@extends('layouts.main')
@section('title', 'Home')

@section('top-resource')
@endsection

@section('content')
    <div class="container" style="margin-top:7rem;">
        <h1>Forum</h1>
        <hr>
        <div class="row">
            <div class="col-sm-3 col-md-3 col-lg-3">
                <a class="btn btn-primary btn-block mb-3" href="{{ route('thread.create') }}">Create Thread</a>
                {{-- <form class="">
                    <input class="form-control" type="text" placeholder="Enter keyword . . . ." style="border: 1px solid black;">
                    <button class="btn btn-secondary mt-2" type="submit">Search</button>
                </form> --}}
                <hr>
                <h4>Tags</h4>
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Laravel
                        <span class="badge badge-primary badge-pill">14</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Blade
                        <span class="badge badge-primary badge-pill">2</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Cache
                        <span class="badge badge-primary badge-pill">1</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Database
                        <span class="badge badge-primary badge-pill">1</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Lumen
                        <span class="badge badge-primary badge-pill">1</span>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9 col-md-9 col-lg-9">
                @if(session('msg-success'))
                    <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fa fa-check"></i> Success!</h5>
                            {{ session('msg-success') }}
                    </div>
                @endif
                @if(session('msg-delete'))
                    <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fa fa-ban"></i> Alert!</h5>
                            {{ session('msg-delete') }}
                    </div>
                @endif

                @forelse($threads as $thread)
                    <div class="card mb-3">
                        <div class="card-header">
                            <div class="profile">
                                <a href="https://laravel.io/user/sidney%20machara">
                                    <img class="rounded-circle img-fluid" style="width:2rem;" src="https://www.gravatar.com/avatar/09827822f952b6b0e2bbbbe182618d3f?s=25&amp;d=https%3A%2F%2Flaravel.io%2Favatar%2Fsidney%2520machara">
                                </a>
                                <a href="https://laravel.io/user/sidney%20machara">{{ $thread->user->name }}</a>
                                {{ $thread->updated_at }}

                                <a href="https://laravel.io/forum/tags/laravel">
                                    <span class="badge badge-default pull-right">{{ $thread->type }}</span>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <a href="{{ route('thread.show', $thread->id) }}">{{ $thread->subject }}</a>
                        </div>
                    </div>
                @empty
                    No Thread
                @endforelse
            </div>
        </div>

    </div>
@endsection

@section('bottom-resource')

@endsection
